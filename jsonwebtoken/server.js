// The basics
const express = require('express')
const jwt = require('jsonwebtoken')

const app = express()


// Verifying function

getToken = (req, res, next) => {
    const bearer = req.headers['authorization']

    if (typeof bearer != undefined) {
        const bearerToken = bearer.split(' ')[1]
        req.token = bearerToken
        
        next()
    } else {
        res.sendStatus(403)
    }
}


app.post('/test', (req, res) => {

    const token = jwt.sign({ foo: 'bar' }, 'shhhhh', (err, token) => {
        res.json({
            token
        })
    })
})

app.post('/login', getToken, (req, res) => {
    jwt.verify(req.token, 'shhh', (err, decoded) => {
        if (err) {
            res.sendStatus(403)
        } else {
            res.json({decoded})
        }
    })
})

app.listen( 8080, () => {
    console.log('TEST')
})